# Disclaimer

Not an official Nintendo product. Not for commercial use. Just a fun learning experiment.

# Attribution

- [Super Mario 64](https://www.nintendo.com/)
- [Mario's Head](https://skfb.ly/66LTF)
- [Title Theme](https://www.youtube.com/watch?v=pYAjvWCVkH0)
- [Background Image](https://cutewallpaper.org/download.php?file=/21/super-mario-64-background/Supper-Mario-Broth-Tileable-Super-Mario-64-title-screen-.png)
- [Coin Sprites](https://mario-bros.fandom.com/wiki/Coin)
- [Sparkle Sprites](http://www.mariouniverse.com/sprites-n64-sm64/)

# License

Files in `src/shaders` contain their own licenses, everything else in `src/` is original work under MIT license. Refer to the attribution links above for licensing on assets in `public/`
